//Context
//Create a context
//Provide a Context value
//consume the context value
import React from 'react'
const UserContext=React.createContext("jeetega bhai jeetega")
//default value can be passed as an argument to the createElement()
const UserProvider = UserContext.Provider
const UserConsumer = UserContext.Consumer
export { UserProvider,UserConsumer }
export default UserContext