import React from 'react';
// import HoverCounter from './components/HoverCounter'
import './App.css';
import Router from './Routing/Router';
import Home from './Routing/Home';
import About from './Routing/About';
import Contact from './Routing/Contact';
import {BrowserRouter as Routers,Switch,Route} from 'react-router-dom'
// import PostHTTP from './components/PostHTTP';
// import GetHTTP from './components/GetHTTP'
// import ComponentB from './components/ComponentB'
// import Input from './components/Input'
// import User from './components/user'
// import FocusInput from './components/FocusInput'
// import HoverCounter2 from './components/hoverCounter2'
// import ClickCounter2  from './components/clickCounter2'
// import Counter from './components/Counter'
// import { UserProvider } from './Context';
// import Refs from './components/Refs'
// import PortalDemo from './components/PortalDemo'
// import ErrorBoundry from './components/ErrorBountry'
// import Hero from './components/Hero'
// import FrParentInput from './components/FrParentInput'
// import ClickCounter from './components/ClickCounter'
function App() {
  return (
    <div className="App">
   {/* <Refs /> */}
   {/* <Input name='K' /> */}
   {/* <Hero heroname="spiderman" />
   <Hero heroname="ironman" />
   <ErrorBoundry>
   <Hero heroname="joker" />
   </ErrorBoundry>
    <FocusInput />
    <FrParentInput />
    <PortalDemo /> */}
    {/* <HoverCounter names='ramesh'/>
    <ClickCounter />
    <ClickCounter2 />
    <HoverCounter2 />
    <User render={(isLoggedIN)=> isLoggedIN?"chandra shekhar":"Guest"} /> */}
    {/* <Counter render={(count,incrementValue)=>{
      return <ClickCounter2 count={count} incrementValue={incrementValue} />
    }} /> */}
    {/* <Counter>
    {(count,incrementValue)=>{
      return <ClickCounter2 count={count} incrementValue={incrementValue} />
    }
    }
    </Counter>
      
    <Counter>
    {(count,incrementValue)=>{
    return   <HoverCounter2 count={count} incrementValue={incrementValue} />
    }} 
    </Counter>
    <UserProvider value="Chnadrt#fghhh">
    <ComponentB />
    </UserProvider> */}
    {/* <GetHTTP /> */}
    {/* <PostHTTP /> */}
    <Routers>
    <Router />
    <Switch>
    <Route path='/' exact component={Home} />
    <Route path='/about' component={About} />
    <Route path='/contact' component={Contact} />
    </Switch>
    </Routers>
    </div>
  );
}

export default App;
