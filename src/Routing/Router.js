import React, { Component } from 'react'
import {Link} from 'react-router-dom'
export class Router extends Component {
    render() {
        const sty={
            color:'white' ,
            background:'black',
            padding:'10px',
            listStyle:'none' ,
            borderTop:'3px solid white'

        }
        return (
            <>
              <nav>
              <ul>
             <Link to='/'>
              <li style={sty}>Home</li>
              </Link>
              <Link to='/about'>
              <li style={sty}>About</li>
              </Link>
              <Link to='/contact'>
              <li style={sty}>Contact</li>
             </Link>
              </ul>
              </nav>  
            </>
        )
    }
}

export default Router
