import React, { Component } from 'react'
import Input from './Input'

class FocusInput extends Component {
    constructor(props) {
        super(props)
       this.ComponentRef=React.createRef()
    }
    clickHandler=()=>{
        // console.log(this.ComponentRef.current.inputRef.current.focus())
        this.ComponentRef.current.focusInput()

    }
    handle=()=>{
        this.ComponentRef.current.hideIt()
    }
    render() {
        return (
            <div>
            <Input ref={this.ComponentRef} />
            <button onClick={this.clickHandler}  onDoubleClick={this.handle} >Click here  </button>                
            </div>
        )
    }
}

export default FocusInput
