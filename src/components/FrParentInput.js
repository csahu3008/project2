import React, { Component } from 'react'
import FrInput from './FrInput'
class FrParentInput extends Component {
    constructor(props){
   super(props)
    this.inputRef=React.createRef()
    }
    clickHandler=()=>{
        console.log(this.inputRef)
    }
    render() {
        return (
            <div>
             <FrInput ref={this.inputRef} /> 
             <button onClick={this.clickHandler}>Focus Input</button>  
            </div>
        )
    }
}

export default FrParentInput
