import React, { Component } from 'react'

class Counter extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             count:0
        }
    }
    incrementValue=()=>{
        this.setState(prevstate=>{
            return{ count:prevstate.count+1}
        })
    }
    render() {
        return (
            <div>
                {this.props.children(this.state.count,this.incrementValue)}
            </div>
        )
    }
}

export default Counter
