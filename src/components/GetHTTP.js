import React, { Component } from 'react'
import axios from 'axios'
class GetHTTP extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             posts:[],
             errorMessage:''
        }
    }
    componentDidMount(){
        axios.get('https://jsonplaceholder.typicode.com/posts').then((response)=>{
            console.log(response)
            this.setState({
                posts:response.data
            })
        }).catch((error)=>{
             console.log(error)
             this.setState({
                 errorMessage:"error in retrieving data"
             })
        })
    }
    render() {
        const {posts,errorMessage}=this.state
        return (
            <div> 
            <h1>List of posts</h1>
            {
                posts.length?posts.map((i)=><React.Fragment key={i.id}><h3>{i.id}</h3><p>{i.title}</p></React.Fragment>):errorMessage
            }
            </div>
        )
    }
}

export default GetHTTP
