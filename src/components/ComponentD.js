import React, { Component } from 'react'
import { UserConsumer } from '../Context'

export class ComponentD extends Component {
    render() {
        return (
            <UserConsumer>
             {
                 (username)=> <div>Hello {username}</div>
             }
             </UserConsumer>  
        )
    }
}

export default ComponentD
