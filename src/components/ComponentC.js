import React, { Component } from 'react'
import ComponentD from './ComponentD'
import UserContext from '../Context'
class ComponentC extends Component {
    static contextType=UserContext
    render() {
        return (
            <div>
            {this.context}
            ComponentC
                <ComponentD />
            </div>
        )
    }
}
// ComponentC.contextType=UserContext
export default ComponentC
