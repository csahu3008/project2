import React, { Component } from 'react'

class Input extends Component {
    constructor(props)
    {
        super(props)
        this.inputRef=React.createRef()
    }
    focusInput() {
        console.log(this.inputRef);
        this.inputRef.current.focus()
    }
    hideIt(){
        this.inputRef.current.blur()
    }
    render() {
        return (
            <div>
             <input type='text' ref={this.inputRef} />   
            </div>
        )
    }
}

export default Input
