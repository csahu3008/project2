import React, { Component } from 'react'

class clickCounter2 extends Component {    
    render() {
         const {count,incrementValue}=this.props
        return (
            <div>
                <button onClick={incrementValue}>Clicked {count} times</button>
            </div>
        )
    }
}

export default clickCounter2
