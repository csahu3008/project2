//higher componenets are used inoreder to reuse the same code in the components
//to share the common functionalites between Components
//HOC - A pattern where a function takes a component as an argument and returns a new component
//const NewComponent=higherOrderComponent(originalComponent)

import React from 'react'
const withCounter=(OriginalComponent,incrementNumber)=>{
     class WithCounter extends React.Component{
        constructor(props) {
            super(props)
        
            this.state = {
                 count:0
            }
        }
        clickHadler=()=>{
            this.setState((prevstate)=>{
                return { count : prevstate.count +incrementNumber }
            })
        }
        
         render(){
           return <OriginalComponent   count={this.state.count} clickHandler={this.clickHadler} {...this.props} />
         }
     }
     return WithCounter
}
export default withCounter