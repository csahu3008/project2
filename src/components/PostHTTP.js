import React, { Component } from 'react'
import axios from 'axios'
class PostHTTP extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             username:'',
             email:'',
             password:''
        }
    }
    Handler1=(event)=>{
        this.setState({
            username:event.target.value
        })
    }
    Handler2=(event)=>{
        this.setState({
            email:event.target.value
        })
    }
    Handler3=(event)=>{
        this.setState({
            password:event.target.value
        })
    }
    SubmitForm=(event)=>{
         event.preventDefault();
         axios.post("url",this.state).then((response)=>{
                console.log(response)
         })
         console.log(this.state)
    }
    render() {
        const {username,email,password}=this.state
        return (
            <div>
                <form onSubmit={this.SubmitForm}>
                <div>
                <label>USERNAME</label>
                <input type='text' value={username} onChange={this.Handler1} />
                </div>
                <div>
                <label>EMAIL</label>
                <input type='text' value={email} onChange={this.Handler2} />
                </div>
                <div>
                <label>PASSWORD</label>
                <input type='password' value={password} onChange={this.Handler3}/>
                </div>
                <input type='submit'  value='submit' />
                </form>
            </div>
        )
    }
}

export default PostHTTP
