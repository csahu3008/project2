import React, { Component } from 'react'
import withCounter from './HigherComponent'
class ClickCounter extends Component {   
    
    render() {
        const {name,count,clickHandler}=this.props
        return (
            <div>
                <button onClick={clickHandler}>{name} Clicked {count} times</button>
            </div>
        )
    }
}
export default withCounter(ClickCounter,10)
