import React, { Component } from 'react'
import withCounter from './HigherComponent'
class HoverCounter extends Component { 
      
    render() {
        const { names,count,clickHandler }=this.props 
        return (
            <div>
                <h1 onMouseOver={clickHandler}> {names} Clicked {count} times</h1>
            </div>
        )
    }
}

export default withCounter(HoverCounter,5)
