//refs make it possible to access the DOM nodes directly
import React, { Component } from 'react'

class Refs extends Component {
    constructor(props)
    {
        super(props)
        this.inputRef=React.createRef()
    }
    componentDidMount(){
        this.inputRef.current.focus()
        console.log(this.inputRef)
    }
    getVal=()=>alert(this.inputRef.current.value)
    render() {
        return (
            <div>
                <form>
                    <label>Username</label>
                    <input type='text' ref={this.inputRef} />
                    <input type='button' value='get' onClick={this.getVal} />
                </form>
            </div>
        )
    }
}

export default Refs
