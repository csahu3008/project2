//RENDER PROPS
//the term render props refers to a technique  of sharing code between React Components using a props whose value is a function
import React, { Component } from 'react'

 class user extends Component {
    render() {
        return (
            <div>
                +++@{this.props.render(true)}@+++
            </div>
        )
    }
}

export default user
