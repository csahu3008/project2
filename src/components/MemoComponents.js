//similar to pure components in class we have function based memo
//it does not rerenders the component when the value was not updated
import React from 'react'

function MemoComponents({name}) {
    return (
        <div>
            dfghjk{name}
        </div>
    )
}

export default React.memo(MemoComponents)
