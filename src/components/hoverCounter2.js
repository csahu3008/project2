import React, { Component } from 'react'

class hoverCounter2 extends Component {
    render() {
       const {count,incrementValue}=this.props
        return (
            <div>
                <h2 onMouseOver={incrementValue}>Hovered {count} times </h2>
            </div>
        )
    }
}

export default hoverCounter2
